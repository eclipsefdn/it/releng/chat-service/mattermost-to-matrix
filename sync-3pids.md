# sync 3PID script

## Set admin permission 

UPDATE users SET admin = 1 WHERE name = '@sebastien.heurtematte:openhw.matrix.eclipsecontent.org';

## Set rate limit

insert into ratelimit_override values ('@sebastien.heurtematte:openhw.matrix.eclipsecontent.org', 0, 0);

## Open tunnel

~/workspace/workspace-matrix/chat-service-administration/synapse-admin/okd-c2

sudo ./tunnel.sh openhw $HOME

./admin.sh openhw $HOME

## Run script

```
npm install
node sync-3pids.js
```