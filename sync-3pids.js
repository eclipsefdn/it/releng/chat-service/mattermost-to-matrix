const axios = require('axios');
const https = require('https');
const users = require('./downloaded/users.json');

const matrixServer = 'openhw.matrix.eclipsecontent.org';
const accessToken = 'XXXXXXXXXXXXXXXXXXXXXX';

// Fonction pour récupérer le user depuis Matrix
const axiosInstance = axios.create({
    baseURL: `https://${matrixServer}`,
    headers: {
        'Authorization': `Bearer ${accessToken}`,
        'Content-Type': 'application/json'
    },
    httpsAgent: new https.Agent({
        rejectUnauthorized: false // Mode insecure
    })
});

async function getUserFromMatrix(mxId) {
    console.log(`Get user ${mxId}`);
    try {
        const path = encodeUri("/_synapse/admin/v2/users/$mxId", { $mxId: mxId });
        const response = await axiosInstance.get(path);
        return response.data;
    } catch (error) {
        throw error.response ? error.response.data : error.message;
    }
}

async function updateUserMatrix(mxId, userData) {
    console.log(`Update user ${mxId} with data ${JSON.stringify(userData)}`);
    try {
        const path = encodeUri("/_synapse/admin/v2/users/$mxId", { $mxId: mxId });
        const response = await axiosInstance.put(path, userData);
        return response.data;
    } catch (error) {
        throw error.response ? error.response.data : error.message;
    }
}

function encodeUri(pathTemplate, variables) {
    for (const key in variables) {
        if (!variables.hasOwnProperty(key)) {
            continue;
        }
        const value = variables[key];
        if (value === undefined || value === null) {
            continue;
        }
        pathTemplate = pathTemplate.replace(key, encodeURIComponent(value));
    }
    return pathTemplate;
}


async function updateUsers() {

    for (const user of users) {
        const mxid = `@${user.username}:${matrixServer}`;
        console.log(`Sync user ${user.username}, ${mxid}`);
        try {
            const userData = await getUserFromMatrix(mxid);
            userData.threepids.push(
                {
                    "medium": "email",
                    "address": user.email,
                }
            );
            (userData.avatar_url == null) && delete userData.avatar_url;

            const updateResponse = await updateUserMatrix(mxid, userData);

            console.log(`Update succeed ${mxid}`);
            // console.log(updateResponse);
        } catch (error) {
            console.error(`Error updating ${mxid}`);
            console.error(error);
        }
    }
}

updateUsers();
